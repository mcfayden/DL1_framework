from __future__ import print_function
import numpy as np
import h5py
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import argparse
import pandas as pd

parser = argparse.ArgumentParser(description=' Options for variable plotting')

parser.add_argument('-i', '--input_file', type=str,
                    default='train_short.h5',
                    help='''Enter the name of preprocessed file from which you
                    want to plot''')

parser.add_argument('-w', '--weight_file', type=str,  # to be changed to bool!
                    default='',
                    help='Enter the name of preprocessed weight file')

parser.add_argument('-o', '--output_file', type=str,
                    default='hybrid_Calo_WBin.pdf',
                    help='Enter the name of output file')


args = parser.parse_args()

f = h5py.File(args.input_file, 'r')


bjet = pd.DataFrame(f['train_processed_bjets'][:])
cjet = pd.DataFrame(f['train_processed_cjets'][:])
ujet = pd.DataFrame(f['train_processed_ujets'][:])
if args.weight_file != '':
    weights = [ujet['weight'], cjet['weight'], np.ones(len(bjet))]
else:
    weights = [np.ones(len(ujet)), np.ones(len(cjet)), np.ones(len(bjet))]

variablelist = list(bjet.columns.values)
print('Var Number = %i' % len(variablelist))
variablelist.remove('label')
# variablelist.remove('weight')

fig = plt.figure(figsize=(15, 40))
for i, var in enumerate(variablelist):
    if "isDefaults" in var:
        nbins = 2
    else:
        nbins = 50
    plt.subplot(10, 5, i + 1)
    plt.hist([ujet[var], cjet[var], bjet[var]], nbins,  # normed=1,
             weights=weights,
             color=['#4854C3', '#97BD8A', '#D20803'],
             label=['ujets', 'cjets', 'bjets'], histtype='step', stacked=False,
             fill=False)
    plt.yscale('log')
    plt.title(var)
    plt.legend()

plt.tight_layout()
plt.savefig(args.output_file)
