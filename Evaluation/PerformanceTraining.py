"""
 ******************************************************************************
 * Project: DL1_framework                                                     *
 * Package: atlas-flavor-tagging-tools                                        *
 *                                                                            *
 * Description:                                                               *
 *      Perofmance evaluation script for DL1 tagger                           *
 *                                                                            *
 * Authors:                                                                   *
 *      ATLAS flavour tagging group, CERN, Geneva                             *
 *                                                                            *
 ******************************************************************************
"""
from __future__ import print_function
import numpy as np
import os
import h5py
import sys
sys.path.append('../Training')
from tools_training import Swish

# os.environ['PYTHONHASHSEED'] = '0'

from os import environ
environ['KERAS_BACKEND'] = 'tensorflow'
# environ['OMP_NUM_THREADS'] = '1'
# environ['KERAS_BACKEND'] = 'theano'

from keras.models import load_model
from keras.utils import np_utils
import argparse
parser = argparse.ArgumentParser(
    description=' Options for making the preprocessing file')

parser.add_argument('-m', '--model', type=str,
                    default='../Training/results/keras2_model/model_epoch499.h5',
                    help='Enter the name of model to be evaluated')

parser.add_argument('-d', '--output_dir', type=str,
                    default="roc_curves/keras2_model",
                    help='''set the output directory, where the results are
                        stored''')

parser.add_argument('-o', '--output_file', type=str,
                    default="model_results499.h5",
                    help='Set name of output file')

parser.add_argument('-v', '--validation_file', type=str,
                    default="/data/mg294/temp_tuples/DL1validation_ttbar_MC16D_reduced_odd_renamed_preprocessed.h5",
                    help='Set name of preprocessed input validation file')

args = parser.parse_args()
# make sure that the output path has the right format
args.output_dir = os.path.abspath(args.output_dir)
if not os.path.exists(args.output_dir):
    os.makedirs(args.output_dir)

nb_classes = 3
b_index, c_index, u_index = 2, 1, 0

number_of_x_steps = 500

target_beff = 0.77
var_remove = ['label', 'weight']

f = h5py.File(args.validation_file, 'r')
print('loading dataset: ', args.validation_file)

train_bjet = f['train_processed_bjets'][:]
train_cjet = f['train_processed_cjets'][:]
train_ujet = f['train_processed_ujets'][:]

print('dataset loaded. ')
var_names = list(train_bjet.dtype.names)

for elem in var_remove:
    if elem in var_names:
        print("removing variable '", elem, "' for evaluation")
        var_names.remove(elem)
    else:
        print("Tried to remove the variable ", elem,
              " but cannot find it!")

train_bjet = train_bjet[var_names]
train_bjet = np.array(train_bjet.tolist())
train_cjet = train_cjet[var_names]
train_cjet = np.array(train_cjet.tolist())
train_ujet = train_ujet[var_names]
train_ujet = np.array(train_ujet.tolist())

X_train = np.concatenate((train_ujet, train_cjet, train_bjet))
X_train = np.array(X_train.tolist())
y_train = np.concatenate((np.zeros(len(train_ujet)), np.ones(len(train_cjet)),
                          2 * np.ones(len(train_bjet))))
y_train = np.array(y_train.tolist())

print('y_train.shape', y_train.shape)
Y_train = np_utils.to_categorical(y_train, nb_classes)


def MakeRejection(b_jets, c_jets, u_jets):

    c_eff_list = []
    u_eff_list = []
    cut_x_list = []
    cut_y_list = []

    total_c_jets = len(c_jets)
    total_u_jets = len(u_jets)

    percent_shown = 0

    for b_frac in np.linspace(0, 1, number_of_x_steps):

        percent = int(((b_frac) * 100.0) / (1.0))

        if(percent % 10 == 0 and percent > percent_shown):
            print(str(percent) + ' percent done')
            percent_shown = percent

        bscores = np.log(b_jets[:, b_index] /
                         (b_frac * b_jets[:, c_index] + (1 - b_frac) *
                          b_jets[:, u_index]))

        cutvalue = np.percentile(bscores, 100.0 * (1.0 - target_beff))

        # print ('f, cutval = ', b_frac, cutvalue)
        c_eff = len(c_jets[np.log(c_jets[:,b_index] / (b_frac * c_jets[:, c_index] + (1 - b_frac) * c_jets[:, u_index])) > cutvalue]) / float(total_c_jets)
        u_eff = len(u_jets[np.log(u_jets[:, b_index] / (b_frac * u_jets[:, c_index] + (1 - b_frac) * u_jets[:, u_index])) > cutvalue]) / float(total_u_jets)

        # c_eff = len( c_jets[np.log(c_jets[:, b_index] /
        #                            (b_frac * c_jets[:, c_index] + (1 - b_frac) *
        #                             c_jets[:, u_index])) > cutvalue ] ) / (
        #                                 float(total_c_jets) )
        # u_eff = len( u_jets[np.log(u_jets[:, b_index] /
        #                            (b_frac * u_jets[:, c_index] + (1 - b_frac)*
        #                             u_jets[:, u_index])) > cutvalue ] ) / (
        #                                 float(total_u_jets))

        # print ('f, cutval, c_eff, u_eff = ', b_frac, cutvalue, c_eff, u_eff)
        c_eff_list.append(c_eff)

        u_eff_list.append(u_eff)
        cut_x_list.append(b_frac)
        cut_y_list.append(cutvalue)

    c_rej = [1 / x for x in c_eff_list]
    u_rej = [1 / x for x in u_eff_list]

    return c_rej, u_rej, cut_x_list, cut_y_list
# ----- MakeRejction ------ #


def GetGraph(loaded_model):

    b_jets = loaded_model.predict(train_bjet, batch_size=500, verbose=0)
    c_jets = loaded_model.predict(train_cjet, batch_size=500, verbose=0)
    u_jets = loaded_model.predict(train_ujet, batch_size=500, verbose=0)

    eval_loss = loaded_model.evaluate(X_train, Y_train, batch_size=500,
                                      verbose=0)

    c_rej, u_rej, fraclist, cutlist = MakeRejection(b_jets, c_jets, u_jets)

    return c_rej, u_rej, fraclist, cutlist, b_jets, c_jets, u_jets, eval_loss
# ----- def GetGraph --------- #


h5f = h5py.File("%s/%s" % (args.output_dir, args.output_file), 'w')
print(args.model)
test_model = load_model(args.model, custom_objects={'Swish': Swish})
c_rej, u_rej, fraclist, cutlist, b_jets, c_jets, u_jets, eval_loss = GetGraph(test_model)


h5f.create_dataset('crej', data=c_rej)
h5f.create_dataset('urej', data=u_rej)
h5f.create_dataset('frac', data=fraclist)
h5f.create_dataset('cut', data=cutlist)
h5f.create_dataset('b_jets', data=b_jets)
h5f.create_dataset('c_jets', data=c_jets)
h5f.create_dataset('u_jets', data=u_jets)
# h5f.create_dataset('loss', data=loss_arr)
h5f.create_dataset('eval_loss', data=eval_loss)

h5f.close()
