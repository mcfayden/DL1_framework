"""
 ******************************************************************************
 * Project: DL1_framework                                                     *
 * Package: atlas-flavor-tagging-tools                                        *
 *                                                                            *
 * Description:                                                               *
 *      Preprocessing and weight computation script for DL1 tagger            *
 *                                                                            *
 * Authors:                                                                   *
 *      ATLAS flavour tagging group, CERN, Geneva                             *
 *                                                                            *
 ******************************************************************************
"""
from __future__ import print_function
import h5py
import numpy as np
import os
import json
import pandas as pd
import tools_preprocess as tp
from sample_preparation import RebinSample, PrepareBinnedSample

# retrieves the argparse options from tools_preprocess.py
args = tp.GetParser()


# in test mode, check if dict file exists
if(args.add_dl1):
    if not (os.path.isfile("%s/%s.json" % (args.dict_dir, args.dict_file))):
        print("Scaler file ", "%s/%s.json" % (args.dict_dir, args.dict_file),
              "does not exist.",
              "Run first in training mode --add_dl1 0 or specify dict",
              "file via --dict_file.")
        exit()
print('Preprocessing ', args.input_file)

# ------------------------ load the input data file ------------------------ #
# rebinns and samples down the distributions such that pT and eta are equal for
# all categories
if args.only_scale:
    X = PrepareBinnedSample(input_file=args.input_file, no_weights=True,
                            dummy_weights=args.dummy_weights)
elif args.downsampled:
    X = PrepareBinnedSample(input_file=args.input_file)
else:
    X = RebinSample(input_file=args.input_file, plot_name=args.plot_name,
                    odd_events=args.add_dl1, info_plots=args.info)
    # ------------------- process input variables for DL1 ------------------- #
    # using pandas to simplify the handling of types np ndarray, struct np
    # array extract input variables for DL1, defined in external dict in
    # DL1_Variables.py
    X = pd.DataFrame(X)
    # due to new binning procedure, not necessary to apply weights anymore
    # however the option will be kept if necessary at a leter stage
    X['weight'] = np.ones(len(X))
    # rename label
    X = X.rename(index=str, columns={'HadronConeExclTruthLabelID': 'label'})
    # replace inf and -inf values (occuring in IP2(3)D) with NaN values
X.replace([np.inf, -np.inf], np.nan, inplace=True)
# apply cuts to remove big outliers
# cuti = "JetFitter_N2Tpair<400 & SV1_significance3d<750 & secondaryVtx_E<4e9"
# cuti += "& secondaryVtx_L3d<2000 & secondaryVtx_Lxy<1500"
# cuti += "& min_trk_flightDirRelEta>-0.45"
# X.query(cuti, inplace=True)

print("Apply scaling and shifting")
if args.add_dl1 == 0:
    scale_dict = []
    for var in X.columns.values:
        if var in ['label', 'weight', 'category']:
            continue
        elif 'isDefaults' in var:
            # no scaling and shifting is applied to the check variables
            scale_dict.append(tp.dict_in(var, 0., 1., None))
        else:
            dict_entry = tp.Get_Shift_Scale(vec=X[var].values,
                                            w=X['weight'].values, varname=var)
            scale_dict.append(tp.dict_in(*dict_entry))

    # save scale/shift dictionary to json file
    scale_name = '%s/%s.json' % (args.dict_dir, args.dict_file)
    with open(scale_name, 'w') as outfile:
        json.dump(scale_dict, outfile, indent=4)
    print("saved scale dictionary as", scale_name)
else:
    with open('%s/%s.json' % (args.dict_dir, args.dict_file), 'r') as infile:
        scale_dict = json.load(infile)
if args.no_writing:
    exit(0)
# Replace NaN values with default values from default dictionary
default_dict = tp.Gen_default_dict(scale_dict)
X.fillna(default_dict, inplace=True)
# scale and shift distribution
for elem in scale_dict:
    if 'isDefaults' in elem['name']:
        continue
    else:
        X[elem['name']] = (X[elem['name']] - elem['shift']) / elem['scale']

print('Total number of variables = ', len(X.columns.values) - 2,
      '(w/o label & weights)')
# split samples into b, c and light jet categories
bjets = X.query('label==5').to_records(index=False)

cjets = X.query('label==4').to_records(index=False)

ujets = X.query('label==0').to_records(index=False)


# ------------------------ save output file to disk ------------------------ #
outfile_name = '%s/%s' % (args.output_dir, args.output_file)
print("Save file as ", outfile_name)
h5f = h5py.File(outfile_name, 'w')
h5f.create_dataset('train_processed_bjets', data=bjets)
h5f.create_dataset('train_processed_cjets', data=cjets)
h5f.create_dataset('train_processed_ujets', data=ujets)

if args.info:
    # Plot all preprocessed variable distributions
    print("Plot all preprocessed variable distributions")
    tp.Plot_vars(bjets, cjets, ujets, args.plot_name)

h5f.close()
