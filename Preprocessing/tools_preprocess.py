"""
 ******************************************************************************
 * Project: DL1_framework                                                     *
 * Package: atlas-flavor-tagging-tools                                        *
 *                                                                            *
 * Description:                                                               *
 *      preprocessing tools for DL1 tagger                                    *
 *                                                                            *
 * Authors:                                                                   *
 *      ATLAS flavour tagging group, CERN, Geneva                             *
 *                                                                            *
 ******************************************************************************
"""
from __future__ import print_function
import numpy as np
import pandas as pd
import os
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import argparse
from DL1_Variables import custom_defaults_vars


pt_bins = np.concatenate((np.linspace(0, 600000, 351),
                          np.linspace(650000, 2000000, 28)))
eta_bins = np.linspace(0, 2.5, 26)


def retr_weights(row, cweight_hist, uweight_hist):
    """Application of event weight"""
    if int(row['label']) == 5:
        return 1.

    locations_pt = np.digitize(row['pt_uncalib'], pt_bins)
    locations_eta = np.digitize(row['abs_eta_uncalib'], eta_bins)

    locations_pt[locations_pt >= len(pt_bins) - 1] = len(pt_bins) - 2
    locations_eta[locations_eta >= len(eta_bins) - 1] = len(eta_bins) - 2

    if int(row['label']) == 4:
        c_weight = cweight_hist[(locations_eta, locations_pt)]
        return c_weight
    elif int(row['label']) == 0:
        u_weights = uweight_hist[(locations_eta, locations_pt)]
        return u_weights


def GetWeights(in_array, c_label=4, u_label=0, b_label=5):
    """Calculation of event weights."""

    arr_b = in_array.query('label==%i' % b_label)
    arr_c = in_array.query('label==%i' % c_label)
    arr_u = in_array.query('label==%i' % u_label)

    histvals_b = np.histogram2d(arr_b['abs_eta_uncalib'], arr_b['pt_uncalib'],
                                [eta_bins, pt_bins])
    histvals_c = np.histogram2d(arr_c['abs_eta_uncalib'], arr_c['pt_uncalib'],
                                [eta_bins, pt_bins])
    histvals_u = np.histogram2d(arr_u['abs_eta_uncalib'], arr_u['pt_uncalib'],
                                [eta_bins, pt_bins])
    histvals_c = histvals_c[0]
    histvals_b = histvals_b[0]
    histvals_u = histvals_u[0]

    histvals_b[histvals_b == 0] = 1
    histvals_c[histvals_c == 0] = 1
    histvals_u[histvals_u == 0] = 1

    cweight_hist = np.divide(histvals_b, histvals_c)
    uweight_hist = np.divide(histvals_b, histvals_u)
    del arr_b, arr_c, arr_u
    weights = in_array.apply(retr_weights, cweight_hist=cweight_hist,
                             uweight_hist=uweight_hist, axis=1)
    return weights.values


def Get_Shift_Scale_bak(vec, w, varname):
    """Calculates the weighted average and std for vector vec and weight w."""
    # NaN values are not considered in calculation
    w = w[~np.isnan(vec)]
    vec = vec[~np.isnan(vec)]
    average = np.ma.average(vec, weights=w)
    std = np.sqrt(np.average((vec - average) ** 2, weights=w))
    return average, std


def Get_Shift_Scale(vec, w, varname):
    """Calculates the weighted average and std for vector vec and weight w."""
    # find NaN values
    nans = np.isnan(vec)
    # check if variable has predefined default value
    if varname in custom_defaults_vars:
        default = custom_defaults_vars[varname]
    # NaN values are not considered in calculation for average
    else:
        w_without_nan = w[~nans]
        vec_without_nan = vec[~nans]
        default = np.ma.average(vec_without_nan, weights=w_without_nan)
    # replace NaN values with default values
    vec[nans] = default
    average = np.ma.average(vec, weights=w)
    std = np.sqrt(np.average((vec - average) ** 2, weights=w))
    return [varname, average, std, default]


def dict_in(varname, average, std, default):
    """Creates dictionary entry containing scale and shift parameters."""
    return {"name": varname, "shift": average, "scale": std,
            "default": default}


def Gen_dict(avg, std, vars, scale_vars):
    """Combines dictionary entries."""
    scale_dict = []
    for var in vars:
        if var in ['label', 'weight']:
            continue
        elif 'isDefaults' in var:
            scale_dict.append(dict_in(var, 0., 1., None))
            continue

        var_pos = scale_vars.index(var)
        if var in custom_defaults_vars:
            scale_dict.append(dict_in(var, avg[var_pos], std[var_pos],
                              custom_defaults_vars[var]))
            continue
        else:
            scale_dict.append(dict_in(var, avg[var_pos], std[var_pos],
                              avg[var_pos]))
    return scale_dict


def Gen_default_dict(scale_dict):
    """Generates default value dictionary from scale/shift dictionary."""
    default_dict = {}
    for elem in scale_dict:
        if 'isDefaults' in elem['name']:
            continue
        default_dict[elem['name']] = elem['default']
    return default_dict


def GetParser():
    """Argparse option for Preprocessing script."""
    parser = argparse.ArgumentParser(description=""" Options for making the
                                     preprocessing file""")

    parser.add_argument('-i', '--input_file', type=str,
                        required=True,
                        help="""Enter the name of hybrid training or ttbar
                        test sample""")

    parser.add_argument('-a', '--add_dl1', type=int,
                        default=0,
                        choices=[0, 1],
                        help="""set 0 for training sample and 1 for test
                        sample""")

    parser.add_argument('-n', '--nentries', type=int,
                        default=10220000,
                        help='set the input number of entries')

    parser.add_argument('-d', '--output_dir', type=str,
                        default=".",
                        help='set the output directory')

    parser.add_argument('-o', '--output_file', type=str,
                        required=True,
                        help="""Set name of output file.""")

    parser.add_argument('-f', '--dict_file', type=str,
                        default=None,
                        help="""Name of json file containing scaling, shift and
                        default values.""")
    parser.add_argument('-v', '--var_file', type=str,
                        default=None,
                        help="""Name of json file containing list of input
                        variables for tagger.""")

    parser.add_argument('-c', '--dict_dir', type=str,
                        default="dicts",
                        help='''set the output directory for the dictionary
                        files.''')

    parser.add_argument('-p', '--plot_name', type=str,
                        default="InfoPlot",
                        help='''Defines the name of the info plots.''')

    parser.add_argument('--info', action='store_true', help='''Creates
                        additional plots to check results.''')
    parser.add_argument('--downsampled', action='store_true',
                        help='''Input file already downsampled.''')
    parser.add_argument('--only_scale', action='store_true',
                        help='''Input file already downsampled/weighted.
                        Only scaling and shifting needs to be applied''')
    parser.add_argument('--dummy_weights', action='store_true',
                        help='''Input file already downsampled and no weights
                        necessary -> weights will be set to 1.''')
    parser.add_argument('--no_writing', action='store_true',
                        help='''Only retrieves scaling and shifting values.
                        And does not write out the file only the dict file.''')
    parser.add_argument('--large_file', action='store_true',
                        help='''Only for Apply_Scale script. Creates training
                        input for DataGenerator for large files.''')
    parser.add_argument('--preprocessed', action='store_true',
                        help='''Only for Apply_Scale script wih --large_file
                        option. Indicates that input file is already
                        preprocessed. (with Apply_scale script w/o large_file
                        option)''')
    # make sure that the output path has the right format
    args = parser.parse_args()
    args.output_dir = os.path.abspath(args.output_dir)
    args.dict_dir = os.path.abspath(args.dict_dir)
    if not os.path.exists(args.dict_dir):
        os.makedirs(args.dict_dir)
    return args


def MakePlots(bjets, ujets, cjets, plot_name="InfoPlot", option='',
              binning={"pt_uncalib": np.linspace(10000, 2000000, 200),
                       "abs_eta_uncalib": np.linspace(0, 2.5, 26)}):
    """Plots pt and eta distribution."""


    vars = ["pt_uncalib", "abs_eta_uncalib"]

    for i, var in enumerate(vars):
        plt.subplot(1, 2, i + 1)
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 3),
                             useMathText=True)
        plt.hist([ujets[var], cjets[var], bjets[var]], binning[var],
                 # weights=[arr_u['weight'], arr_c['weight'],
                 #          np.ones(len(arr_b))],
                 # color=['#4854C3', '#97BD8A', '#D20803'],
                 # color=['#2ca02c', '#1f77b4', '#d62728'],
                 color=['#2ca02c', '#ff7f0e', '#1f77b4'],
                 # color=['forestgreen', 'mediumblue', 'r'],alpha=0.8,
                 label=['ujets', 'cjets', 'bjets'], histtype='step',
                 stacked=False, fill=False)
        plt.yscale('log')
        plt.title(var)
        plt.legend()
    plt.tight_layout()
    if not os.path.exists(os.path.abspath("./plots")):
        os.makedirs(os.path.abspath("./plots"))
    plt.savefig("plots/%s_%s_pt-eta.pdf" % (plot_name, option),
                transparent=True)
    plt.close()


def Plot_vars(bjets, cjets, ujets, plot_name="InfoPlot"):
    """Creates plots of all variables and saves them."""
    bjet = pd.DataFrame(bjets)
    cjet = pd.DataFrame(cjets)
    ujet = pd.DataFrame(ujets)
    variablelist = list(bjet.columns.values)
    print(variablelist)
    print(len(variablelist))
    variablelist.remove('label')
    variablelist.remove('weight')
    if 'category' in variablelist:
        variablelist.remove('category')

    plt.figure(figsize=(20, 60))
    for i, var in enumerate(variablelist):
        if "isDefaults" in var:
            nbins = 2
        else:
            nbins = 50
        plt.subplot(20, 5, i + 1)
        plt.hist([ujet[var], cjet[var], bjet[var]], nbins,  # normed=1,
                 weights=[ujet['weight'], cjet['weight'], bjet['weight']],
                 # color=['#4854C3', '#97BD8A', '#D20803'],
                 # color=['#2ca02c', '#1f77b4', '#d62728'],
                 color=['#2ca02c', '#ff7f0e', '#1f77b4'],
                 label=['ujets', 'cjets', 'bjets'], histtype='step',
                 stacked=False, fill=False)
        plt.yscale('log')
        plt.title(var)
        plt.legend()
    plt.tight_layout()
    plotname = "plots/%s_all_vars.pdf" % plot_name
    print("save plot as", plotname)
    plt.savefig(plotname, transparent=True)
