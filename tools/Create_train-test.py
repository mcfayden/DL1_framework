import pandas as pd
import h5py

even = True
odd = False

path = "/work/ws/atlas/mg294-b_tagging/ntuples_2018/PFlow/ttbar_ROOT/\
h5_version"
in_file = "ttbar_PFlow-newIPtag-merged.h5"
h5f = h5py.File("%s/%s" % (path, in_file), 'r')
jets = pd.DataFrame(h5f['jets'][:])
if odd:
    jets.query('eventNumber % 2 != 0', inplace=True)
    # jets.query('HadronConeExclTruthLabelID % 2 != 0', inplace=True)
    outfile_name = path + "/ttbar_PFlow-newIPtag-merged-odd.h5"
    h5f = h5py.File(outfile_name, 'w')
    h5f.create_dataset('jets', data=jets.to_records(index=False))
    h5f.close()


if even:
    jets.query('eventNumber % 2 == 0', inplace=True)
    outfile_name = path + "/ttbar_PFlow-newIPtag-merged-even.h5"
    h5f = h5py.File(outfile_name, 'w')
    h5f.create_dataset('jets', data=jets.to_records(index=False))
    h5f.close()
